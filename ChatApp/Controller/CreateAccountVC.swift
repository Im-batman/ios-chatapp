//
//  CreateAccountVC.swift
//  ChatApp
//
//  Created by Asar Sunny on 17/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController {

    //outlets
    
   
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var userImage: UIImageView!
    //variables
    var avaratName = "profileDefault"
    var avatarColor = "[0.5, 0.5, 0.5, 1]"
    var bgColor: UIColor?
    
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDataService.instance.avatarName != nil{
            userImage.image  =  UIImage(named:UserDataService.instance.avatarName)
            avaratName = UserDataService.instance.avatarName
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()

        // Do any additional setup after loading the view.
    }

    
    func setupView(){
        activityIndicator.isHidden = true
        
        //use Nsattributed string to change the color
        usernameTxt.attributedPlaceholder = NSAttributedString(string: "username", attributes: [NSAttributedStringKey.foregroundColor: SMACK_COLOR])
        
        emailTxt.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor: SMACK_COLOR])
        
        passwordTxt.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor: SMACK_COLOR])
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(CreateAccountVC.hideKeyboard))
        view.addGestureRecognizer(tap)
        
        
    }
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func pickAvatarPressed(_ sender: Any) {
        performSegue(withIdentifier: TO_AVATAR_PICKER, sender: nil)
        
    }
    @IBAction func generateColorpressed(_ sender: Any) {
        
        let r = CGFloat(arc4random_uniform(255)) / 255
        let g = CGFloat(arc4random_uniform(255)) / 255
        let b = CGFloat(arc4random_uniform(255)) / 255
        
        bgColor = UIColor(red: r, green: g, blue: b, alpha: 1)
        avatarColor = "[\(r),\(g),\(b),1]"
        
        UIView.animate(withDuration: 0.2){
            self.userImage.backgroundColor = self.bgColor

        }
        
    }
    
    
 
    @IBAction func createAccountBtnPressed(_ sender: Any) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        
        guard let name = usernameTxt.text, usernameTxt.text != "" else{return}
        guard let email = emailTxt.text, emailTxt.text != "" else{return}
        guard let password = passwordTxt.text, passwordTxt.text != "" else{return}
        
        AuthService.instance.registerUser(email: email, password: password) { (success) in
            if success {
                print("user Registered!!")
                
                //loging the user after register.
                AuthService.instance.loginUser(withEmail: email, andPassword: password, completion: { (success) in
                    print("User Logged In", AuthService.instance.AuthToken)
                    
//                    create the user
                    AuthService.instance.createUser(name: name, email: email, avaterColor: self.avatarColor, avatarName: self.avaratName, completion: { (success) in
                        if success{
                            self.activityIndicator.stopAnimating()

                            self.activityIndicator.isHidden = true
                            print(UserDataService.instance.name, UserDataService.instance.avatarName)
                           self.performSegue(withIdentifier: "unWindToChannel", sender: nil)
                            NotificationCenter.default.post(name: USER_DATA_DID_CHANGE, object: nil )
                        }
                    })
                })
            }
        }
        
        
    }
    
    

}
