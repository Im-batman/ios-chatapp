//
//  AddChannelVC.swift
//  ChatApp
//
//  Created by Asar Sunny on 19/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
//import SocketIO

class AddChannelVC: UIViewController {

    //outlets
    @IBOutlet weak var channelNameTxt: UITextField!
    @IBOutlet weak var channelDescTxt: UITextField!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var addButton: RoundedButton!
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
   
    
    func setupView(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(AddChannelVC.dismissCreateChannel(_:)))
        bgView.addGestureRecognizer(tap)
        
        channelNameTxt.attributedPlaceholder = NSAttributedString(string: "Channel Name", attributes: [NSAttributedStringKey.foregroundColor:SMACK_COLOR])
        
        channelDescTxt.attributedPlaceholder = NSAttributedString(string: "Channel Description", attributes: [NSAttributedStringKey.foregroundColor:SMACK_COLOR])
        
    }
    @objc func dismissCreateChannel(_ recognizer:UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    @IBAction func addChannel(_ sender: UIButton) {
        
        guard let channelName = channelNameTxt.text, channelNameTxt.text != "" else {return}
        guard let channelDescription = channelDescTxt.text, channelDescTxt.text != "" else{return}
        
        SocketService.addChannelWithSocket(channelName: channelName, channelDescription: channelDescription) { (success) in
            if success{
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
  
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
