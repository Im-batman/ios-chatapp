//
//  ChannelVC.swift
//  ChatApp
//
//  Created by Asar Sunny on 16/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController, UITableViewDataSource , UITableViewDelegate {
   

    //outlets
    
    @IBOutlet weak var userImage: CircleImage!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    
    
    @IBAction func UnwindtoChannel(segue:UIStoryboardSegue){
//        performSegue(withIdentifier: UNWIND_TO_CHANNEL, sender: nil)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
//        setupUserInfo()
       
        //observer for the notifiaction of login after setting data in model
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelVC.userDataChanged(_:)), name: USER_DATA_DID_CHANGE, object: nil)
        //observer for channel loading
        NotificationCenter.default.addObserver(self, selector: #selector(ChannelVC.channelsLoaded(_:)), name: CHANNELS_LOADED, object: nil)
        
        
        //get the channels from the socket after creation
    
        SocketService.getChannels { (success) in
            if success{
                self.tableView.reloadData()
            }
        }
    
    }

    
    override func viewDidAppear(_ animated: Bool) {
        setupUserInfo()
    }
    
    
    //implementation of user data changed
    @objc func userDataChanged(_:Notification){
        setupUserInfo()
    
    }
    
    @objc func channelsLoaded(_ notif:Notification){
        tableView.reloadData()
    }
    
    func setupUserInfo(){
        
        if AuthService.instance.isLoggedIn{
            loginBtn.setTitle(UserDataService.instance.name, for: .normal)
            userImage.image = UIImage(named:UserDataService.instance.avatarName)
            
            
            userImage.backgroundColor = UserDataService.instance.returnUIColor(component: UserDataService.instance.avatarColor)
            
          
        }else{
            loginBtn.setTitle("Login", for: .normal)
            userImage.image = UIImage(named:"menuProfileIcon")
            userImage.backgroundColor = UIColor.clear
            tableView.reloadData()
        }
        
    }
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
        
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        if AuthService.instance.isLoggedIn{
            let profile = ProfileVC()
            profile.modalPresentationStyle = .custom
            present(profile, animated: true, completion: nil)
            
        }else{
             performSegue(withIdentifier: TO_LOGIN, sender: nil)
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.instance.channels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "channelCell", for: indexPath) as? channelCell{
            let channel = MessageService.instance.channels[indexPath.row]
            cell.configureCell(channel: channel)
            return cell
        }else{
             return UITableViewCell()
        }
       
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = MessageService.instance.channels[indexPath.row]
        print(channel)
        MessageService.instance.selectedChannel = channel
        NotificationCenter.default.post(name: CHANNEL_SELECTED, object: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    
    
    
    @IBAction func addChannelBtnPressed(_ sender: Any) {
        
        if AuthService.instance.isLoggedIn{
            let AddChannel = AddChannelVC()
            AddChannel.modalPresentationStyle = .custom
            present(AddChannel, animated: true, completion: nil)
            
            
        }
    }
    
    
    
    
   

}
