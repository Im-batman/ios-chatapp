//
//  LoginVC.swift
//  ChatApp
//
//  Created by Asar Sunny on 17/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    //outlets
    
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    
    
    func setUpView(){
        activityIndicator.isHidden = true
        usernameTxt.attributedPlaceholder = NSAttributedString(string: "Username", attributes:
            [NSAttributedStringKey.foregroundColor:SMACK_COLOR])
        passwordTxt.attributedPlaceholder = NSAttributedString(string: "Password", attributes:  [NSAttributedStringKey.foregroundColor:SMACK_COLOR])
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closePressed(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dontHaveAccountPressed(_ sender: Any) {
        performSegue(withIdentifier: TO_CREATE_ACCOUNT, sender: nil)
    }
    @IBAction func loginBtnPressed(_ sender: Any) {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        guard let email = usernameTxt.text,usernameTxt.text != "" else{return}
        guard let password  = passwordTxt.text,passwordTxt.text != "" else {return}
        
        
        AuthService.instance.loginUser(withEmail: email, andPassword: password) { (success) in
            if success{
                
//                print("Auth token is ---- \(AuthService.instance.AuthToken)")
                // find the rest of the data like avatarname and color.
                AuthService.instance.findUserByEmail(completion: { (success) in
                    NotificationCenter.default.post(name: USER_DATA_DID_CHANGE, object: nil)
                    
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    
                    self.dismiss(animated: true, completion: nil)
                
                })
            }else{
                
            }
        }
        
        
    }
    
}
