//
//  ChatVC.swift
//  ChatApp
//
//  Created by Asar Sunny on 16/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class ChatVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    //outlets
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var chatLabel: UILabel!
    @IBOutlet weak var messageInput: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    
    
    //variable
    var isTyping = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        sendBtn.isHidden = true

        //to shif the view up when the keyboard appears
        view.bindToKeyboard()
        
        
        
        
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
        //when the first view show if user is loggedIn the update data with notification
        if AuthService.instance.isLoggedIn{
            AuthService.instance.findUserByEmail(completion: { (success) in
                if success{
                    NotificationCenter.default.post(name: USER_DATA_DID_CHANGE, object: nil)
                }
            })
           
        }
        
        
        //observse for user data change
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.userDataChanged), name: USER_DATA_DID_CHANGE, object: nil)
        
        //channel selected observer
        NotificationCenter.default.addObserver(self, selector: #selector(channelSelected(_:)), name: CHANNEL_SELECTED, object: nil)
        
        //listen for incoming messages from socket
        SocketService.getChatMessage { (success) in
            self.tableView.reloadData()
            if MessageService.instance.messages.count > 0{
                let  lastMessageIndex = IndexPath(item: MessageService.instance.messages.count-1, section: 0)
                self.tableView.scrollToRow(at: lastMessageIndex, at: .bottom, animated: false)
                
                
            }
            
            
            
        }
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismisskeyboard))
        view.addGestureRecognizer(tap)
        
        
        
    }
    
    
    @objc func dismisskeyboard(){
        
        view.endEditing(true)
    }
    
    
    
    @objc func userDataChanged(){
        if AuthService.instance.isLoggedIn{
               SocketService.connectSocket()
            onLoginGetMessages()
            
        }else{
            chatLabel.text = "Please Login"
            tableView.reloadData()
        }
        
        
    }
    
    
    @objc func channelSelected(_ notif:Notification){
        UpdateWithChannel()
        
    }
    
    func UpdateWithChannel(){
        chatLabel.text = MessageService.instance.selectedChannel?.channelName ?? "UnCat Channel"
        getChannelMessages()
    }
    
    
    
    
    func onLoginGetMessages(){
        if AuthService.instance.isLoggedIn{
            //load the channels when the chat vc loads.
            MessageService.instance.findAllChannels { (success) in
                
                //set channel
                if MessageService.instance.channels.count > 0{
                    MessageService.instance.selectedChannel = MessageService.instance.channels[0]
                    self.UpdateWithChannel()
                }else{
                    
                    self.chatLabel.text = "No Channels yet.."
                }
                
                
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getChannelMessages(){
       guard let channelId = MessageService.instance.selectedChannel?.id else {return}
        MessageService.instance.findAllMessagesForChannel(channelId: channelId) { (success) in
            if success{
                self.tableView.reloadData()
            }
        }
        
        
        
    }
    
    
    @IBAction func messageInputEditing(_ sender: UITextField) {
        if messageInput.text == ""{
            isTyping = false
            sendBtn.isHidden = true
            
        }else{
            if isTyping == false{
                
                sendBtn.isHidden = false
            }
            
            
            
        }
        
        
    }
    
    
    @IBAction func sendMessagePressed(_ sender: UIButton) {
        if AuthService.instance.isLoggedIn{
            guard let message = messageInput.text, messageInput.text != "" else {return}
            guard let channelId = MessageService.instance.selectedChannel?.id else {return}
            
            SocketService.addMessage(messageBody: message, userId: UserDataService.instance.id, channelId: channelId, completion: { (success) in
                self.messageInput.text = ""
//                self.dismisskeyboard()
                self.messageInput.resignFirstResponder()
            })
            
            
            
            
            
            
        }
        
    }
    
    
    
    
    //manage table data
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageService.instance.messages.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as? MessageCell{
            let message = MessageService.instance.messages[indexPath.row]
            cell.configureCell(message: message)
            return cell
            
        }else{
            return UITableViewCell()
        }
    }
    
    
    
    
    
    
    
    

}
