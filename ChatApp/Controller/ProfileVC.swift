//
//  ProfileVC.swift
//  ChatApp
//
//  Created by Asar Sunny on 18/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    //outlets
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.dismissProfile))
        bgView.addGestureRecognizer(tap)
        
    }
    
    @objc func dismissProfile(){
//        view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeModalPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func logOutPressed(_ sender: Any) {
        UserDataService.instance.logOutUser()
        NotificationCenter.default.post(name: USER_DATA_DID_CHANGE, object: nil)
        dismiss(animated: true, completion: nil)
    }
    
    func setupView(){
        userEmail.text = AuthService.instance.userEmail
        userName.text =  UserDataService.instance.name
        userImage.image = UIImage(named:UserDataService.instance.avatarName)
        userImage.backgroundColor = UserDataService.instance.returnUIColor(component: UserDataService.instance.avatarColor)
        
       
    }
    

}
