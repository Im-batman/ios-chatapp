//
//  MessageCell.swift
//  ChatApp
//
//  Created by Asar Sunny on 21/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
//outlets
//
//    @IBOutlet weak var userImage: UIImageView!
//    @IBOutlet weak var userName: UILabel!
//    @IBOutlet weak var timeStamp: UILabel!
//    @IBOutlet weak var messageBody: UILabel!
//
    
    
    @IBOutlet weak var userImage: CircleImage!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var messageBody: UILabel!
    @IBOutlet weak var timeStamp: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    func configureCell(message:Message){
        
        userImage.image = UIImage(named:message.userAvatar)
        userImage.backgroundColor = UserDataService.instance.returnUIColor(component: message.userAvatarColor)
        userName.text = message.userName
        messageBody.text = message.message
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
