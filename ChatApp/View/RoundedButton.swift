//
//  RoundedButton.swift
//  ChatApp
//
//  Created by Asar Sunny on 17/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
@IBDesignable
class RoundedButton: UIButton {

    @IBInspectable var cornerRadius:CGFloat = 3.0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
        
    }
    
    
    override func awakeFromNib() {
//        super.awakeFromNib()
        self.setUpView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setUpView()
    }
    
    
    
    func setUpView(){
        self.layer.cornerRadius = cornerRadius
    }

}
