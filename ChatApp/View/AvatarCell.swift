//
//  AvatarCell.swift
//  ChatApp
//
//  Created by Asar Sunny on 17/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
enum AvatarType{
    case dark
    case light
}



class AvatarCell: UICollectionViewCell {
    @IBOutlet weak var avatarImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    
    func configureCell(index:Int, avatarType: AvatarType){
        if avatarType == .dark{
            avatarImage.image = UIImage(named:"dark\(index)")
              self.layer.backgroundColor = UIColor.lightGray.cgColor
            
        }else{
             avatarImage.image = UIImage(named:"light\(index)")
              self.layer.backgroundColor = UIColor.gray.cgColor
        }
        
        
    }
    
    // for resizng the columns of a collection view
//    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
//        
//        var numberOfColumn:CGFloat = 3
//        
//        if UIScreen.main.bounds.width > 320{
//            numberOfColumn = 4
//        }
//        
//        let spaceBetweenRows:CGFloat = 10
//        let padding :CGFloat = 40
//        let cellDimension = ((collectionView.bounds.width - padding) - (numberOfColumn - 1) * spaceBetweenRows) / numberOfColumn
//        
//        return CGSize(width: cellDimension, height: cellDimension)
//    }
//    
    
    
    
    func setupView(){
        self.layer.backgroundColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        
    }
    
    
}
