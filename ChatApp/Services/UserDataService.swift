//
//  UserDataService.swift
//  ChatApp
//
//  Created by Asar Sunny on 17/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation

class UserDataService{
    static let instance = UserDataService()
 
    public private(set) var id = ""
    public private(set) var avatarColor = ""
    public private(set) var avatarName = ""
    public private(set) var email = ""
    public private(set) var name = ""
    
    
    
    func setUserData(id:String,avatarColor:String,avatarName:String,email:String,name:String){
        self.id = id
        self.avatarColor = avatarColor
        self.avatarName = avatarName
        self.name = name
        self.email = email
        
    }
    
    func setAvatarName(avatarName:String){
        self.avatarName = avatarName
    }
    
    func returnUIColor(component:String)-> UIColor{
        let scanner = Scanner(string: component)
        let skipped = CharacterSet(charactersIn: "[],]")
        let comma = CharacterSet(charactersIn: ",]")
        
        scanner.charactersToBeSkipped = skipped
        
        var r,g,b,a: NSString?
        scanner.scanUpToCharacters(from: comma, into: &r)
        scanner.scanUpToCharacters(from: comma, into: &g)
        scanner.scanUpToCharacters(from: comma, into: &b)
        scanner.scanUpToCharacters(from: comma, into: &a)
        let defaulColor = UIColor.lightGray
        
        guard let rUnwrapped = r else{return defaulColor}
        guard let gUnwrapped = g else{return defaulColor}
        guard let bUnwrapped = b else{return defaulColor}
        guard let aUnwrapped = a else{return defaulColor}
        
        let rFloat = CGFloat(rUnwrapped.doubleValue)
        let gFloat = CGFloat(gUnwrapped.doubleValue)
        let bFloat = CGFloat(bUnwrapped.doubleValue)
        let aFloat = CGFloat(aUnwrapped.doubleValue)
        
        let newUIColor = UIColor(red: rFloat, green: gFloat, blue: bFloat, alpha: aFloat)
        
        return newUIColor
    }
    
    
    func logOutUser(){
        
        self.id = ""
        self.avatarColor = ""
        self.avatarName = ""
        self.name = ""
        self.email = ""
        
        
        AuthService.instance.AuthToken = ""
         AuthService.instance.userEmail = ""
         AuthService.instance.isLoggedIn = false
        MessageService.instance.clearChannels()
        MessageService.instance.clearMessages()
        
        
    }
    
    
}
