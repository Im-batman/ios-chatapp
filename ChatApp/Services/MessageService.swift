//
//  MessageService.swift
//  ChatApp
//
//  Created by Asar Sunny on 19/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class MessageService{
    static let instance = MessageService()

    var channels = [Channel]()
    var messages = [Message]()
    var selectedChannel: Channel?
    

    func findAllChannels(completion:@escaping CompletionHandler){
        
        Alamofire.request(GET_CHANNELS_URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil{
                guard let data = response.data else{return}
                
                if let json = try? JSON(data: data).array{
                    for item in json!{
                        let name = item["name"].stringValue
                        let description = item["description"].stringValue
                        let id = item["_id"].stringValue
                        
                        let channel = Channel(channelName: name, channelDescription: description, id: id)
                       
                        
                        self.channels.append(channel)
                      
                    }
                    
                    NotificationCenter.default.post(name: CHANNELS_LOADED, object: nil)
                    completion(true)
//                    print("channels include - \(self.channels)")
                    
                }
                
                
//                print(self.channels)
                
            }else{
                completion(false)
            }
        }
    }
    
    func clearChannels(){
        channels.removeAll()
    }
    
    
    
    
    func findAllMessagesForChannel(channelId:String,completion:@escaping CompletionHandler){
        self.clearMessages()
        Alamofire.request("\(GET_MESSAGES_URL)/\(channelId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
            if response.result.error == nil{
                guard let data = response.data else{ return}
                if let json = try? JSON(data: data).array{
                    for item in json!{
                        let message = item["messageBody"].stringValue
                        let channelId = item["channelId"].stringValue
                        let id = item["_id"].stringValue
                        let userName = item["userName"].stringValue
                        let userAvatar = item["userAvatar"].stringValue
                        let userAvatarColor = item["userAvatarColor"].stringValue
                        let timeStamp = item["timeStamp"].stringValue
                        
                        let newMessage = Message(message: message, userName: userName, channelId: channelId, userAvatar: userAvatar, userAvatarColor: userAvatarColor, id: id, timeStamp: timeStamp)
                        
                        self.messages.append(newMessage)
                        
                    }
//                    print(self.messages)
                    completion(true)
                }
                
            }else{
                print("messages error..")
                completion(false)
            }
            
        }
        
        
        
        
        
    }
    
    func clearMessages(){
        messages.removeAll()
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
