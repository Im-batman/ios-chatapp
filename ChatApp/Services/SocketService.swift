//
//  SocketService.swift
//  ChatApp
//
//  Created by Asar Sunny on 19/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
import SocketIO

class SocketService: NSObject {
    static let instance = SocketService()


    static let manager = SocketManager(socketURL: URL(string: BASE_URL)!, config: [.log(false), .compress])
    static let socket = manager.defaultSocket

    
    class func connectSocket(){
        SocketService.manager.config = SocketIOClientConfiguration(arrayLiteral: .connectParams(["Authorization" : "Bearer \(AuthService.instance.AuthToken)","Content-Type" : "Application/json"]), .secure(true))
        socket.connect()
    }
    
    
    
    //close the connection
    class func closeConnetion(){
                SocketService.socket.disconnect()
    }
    
    

    //emitting post
    class func addChannelWithSocket(channelName: String, channelDescription: String, completion:@escaping CompletionHandler){

        print("CHEKING SOCKET CONNECTION 1-ST TIME - \(socket.status)") // This gives me - connecting

             socket.emit("newChannel", channelName,channelDescription)
        
//        socket.on("connect") { _, _ in
//            print("socket connected")
//             socket.emit("newChannel", channelName,channelDescription)
//        }

        print("CHEKING SOCKET CONNECTION 2-ND TIME - \(socket.status)") // This gives me - connecting
        completion(true)
    }
    
    
    
    //check status
    class func isSocketConnected() -> Bool{
        if socket.status == .connected {
            return true
        }
        return false
    }


//    receiving data
        class func getChannels(completion:@escaping CompletionHandler){
            SocketService.socket.on("channelCreated") { (dataArray, Ack) in
                guard let channelName = dataArray[0] as? String else {return}
                guard let channelDescription = dataArray[1] as? String else {return}
                 guard let channelId = dataArray[1] as? String else {return}

                let newChannel = Channel(channelName: channelName, channelDescription: channelDescription, id: channelId)
                MessageService.instance.channels.append(newChannel)
                completion(true)

            }


        }
    
    
    
    
    class func addMessage(messageBody:String,userId:String,channelId:String,completion:@escaping CompletionHandler){
        
        let user = UserDataService.instance
        socket.emit("newMessage", messageBody,userId,channelId,user.name,user.avatarName,user.avatarColor)
        
        completion(true)
        
        
    }
    
    //retrieve messages
    class func getChatMessage(completion:@escaping CompletionHandler){
        
        socket.on("messageCreated") { (dataArray, Ack) in
        
            guard let messageBody = dataArray[0] as? String else {return}
            guard let channelId = dataArray[2] as? String else {return}
            guard let userName = dataArray[3] as? String else {return}
            guard let userAvatar = dataArray[4] as? String else {return}
            guard let userAvatarColor = dataArray[5] as? String else {return}
            guard let id = dataArray[6] as? String else {return}
            guard let timeStamp = dataArray[7] as? String else {return}
        
            
            if channelId == MessageService.instance.selectedChannel?.id && AuthService.instance.isLoggedIn{
                
                
                let newMessage = Message(message: messageBody, userName: userName, channelId: channelId, userAvatar: userAvatar, userAvatarColor: userAvatarColor, id: id, timeStamp: timeStamp)
                
                MessageService.instance.messages.append(newMessage)
                 completion(true)
            }else{
                completion(false)
            }
            
            
           
            
            
        }
        
        
    }

    
    
    
    
    
    
    
    
    
    
    
}
