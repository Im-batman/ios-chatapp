//
//  AuthService.swift
//  ChatApp
//
//  Created by Asar Sunny on 17/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthService{
    static let instance = AuthService()
    
    
    let defaults = UserDefaults.standard
    
    var isLoggedIn: Bool{
        get{
            return defaults.bool(forKey: LOGGED_IN_KEY)
        }
        
        set{
            return defaults.set(newValue, forKey: LOGGED_IN_KEY)
        }
    }
    
    var AuthToken :String{
        get{
            return defaults.value(forKey: TOKEN_KEY) as! String
        }
        set{
            return defaults.set(newValue, forKey: TOKEN_KEY)
        }
        
    }
    
    
    var userEmail : String{
        get{
            return defaults.value(forKey: USER_EMAIL) as! String
        }
        set{
            return defaults.set(newValue, forKey: USER_EMAIL)
        }
        
    }
    
    
    //register user
    func registerUser(email:String,password:String, completion: @escaping CompletionHandler ){
        
        let lowerCaseEmail = email.lowercased()
        let body: [String: Any] = [
            "email" : lowerCaseEmail,
            "password" : password
            
        ]
        
        Alamofire.request(REGISTER_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil{
                completion(true)
            }else{
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
        
        
    }
    
    //loginUser
    
    func loginUser(withEmail email:String, andPassword password:String, completion:@escaping CompletionHandler){
        
        
        let lowerCaseEmail = email.lowercased()
        let body: [String: Any] = [
            "email" : lowerCaseEmail,
            "password" : password
            
        ]
        
        Alamofire.request(LOGIN_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            
            
            if response.result.error == nil{
                
                if let data = response.data,
                    let json = try? JSON(data: data){
                    self.userEmail = json["user"].stringValue
                    self.AuthToken = json["token"].stringValue
                    
                }
                
               
                
                self.isLoggedIn = true
                completion(true)
            }else{
                completion(false)
                print("Loging failed, Something happened!")
            }
        }
        
        
        
    }
    
    
    //createUser
    
    func createUser( name:String, email:String,avaterColor:String,avatarName:String, completion:@escaping CompletionHandler){
        
        
        let lowerCaseEmail = email.lowercased()
        let body: [String: Any] = [
             "name" :name,
            "email" : lowerCaseEmail,
            "avatarName" : avatarName,
            "avatarColor": avaterColor
            
        ]
        
        let Header = [
            "Authorization" : "Bearer \(AuthService.instance.AuthToken)",
            "Content-Type" : "Application/json; charset=utf-8"
        ]
        
        Alamofire.request(CREATE_USER_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: Header).responseJSON { (response) in
            if response.result.error == nil{
                
//                if let data = response.data,
//                    let json = try? JSON(data: data){
//                    let id = json["_id"].stringValue
//                    let color = json["avatarColor"].stringValue
//                    let avatarName = json["avatarName"].stringValue
//                    let email = json["email"].stringValue
//                    let name = json["name"].stringValue
//
//
//                    UserDataService.instance.setUserData(id: id, avatarColor: color, avatarName: avatarName, email: email, name: name)
//
//                }
//

//                OR
                
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                let id = json["_id"].stringValue
                let color = json["avatarColor"].stringValue
                let avatarName = json["avatarName"].stringValue
                let email = json["email"].stringValue
                let name = json["name"].stringValue


                UserDataService.instance.setUserData(id: id, avatarColor: color, avatarName: avatarName, email: email, name: name)
                
                
                completion(true)
            }else{
                
                completion(false)
                print("Could'nt create user")
            }
        }

        
        
    }
    
    //used to find the user details after login details are verfied
    func findUserByEmail(completion:@escaping CompletionHandler){
        
        let Header = [
            "Authorization" : "Bearer \(AuthService.instance.AuthToken)",
            "Content-Type" : "Application/json; charset=utf-8"
        ]
        
        Alamofire.request("\(USER_BY_EMAIL_URL)\(self.userEmail)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: Header).responseJSON { (response) in
            
            if response.result.error == nil{
                guard let data = response.data else {return}
                let json = try! JSON(data: data)
                let id = json["_id"].stringValue
                let color = json["avatarColor"].stringValue
                let avatarName = json["avatarName"].stringValue
                let email = json["email"].stringValue
                let name = json["name"].stringValue
                
                UserDataService.instance.setUserData(id: id, avatarColor: color, avatarName: avatarName, email: email, name: name)
                completion(true)
            }else{
                completion(false)
                print("Could'nt create user")
            }
            
        }
        
        
        
    }
    
    
    
    
    
}
