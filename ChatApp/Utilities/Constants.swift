//
//  Constants.swift
//  ChatApp
//
//  Created by Asar Sunny on 17/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation



typealias CompletionHandler = (_ Success:Bool) -> ()


//url

let BASE_URL = "https://smackchatty-chat.herokuapp.com/v1/"
let REGISTER_URL = "\(BASE_URL)account/register"
let LOGIN_URL = "\(BASE_URL)account/login"
let CREATE_USER_URL = "\(BASE_URL)user/add"
let USER_BY_EMAIL_URL = "\(BASE_URL)user/byEmail/"
let GET_CHANNELS_URL = "\(BASE_URL)channel"
let GET_MESSAGES_URL = "\(BASE_URL)message/byChannel"
//

//color
let SMACK_COLOR = #colorLiteral(red: 0.2588235294, green: 0.3294117647, blue: 0.7254901961, alpha: 0.5125749144)

//Notifications

let USER_DATA_DID_CHANGE = Notification.Name("userDataChanged")
let CHANNELS_LOADED = Notification.Name("channelsLoaded")
let CHANNEL_SELECTED = Notification.Name("channelSelected")




//segues

let TO_LOGIN = "toLogin"
let TO_CREATE_ACCOUNT = "toCreateAccount"
let UNWIND_TO_CHANNEL = "unWindToChannel"
let TO_AVATAR_PICKER = "toAvatarPicker"


//defaults

let LOGGED_IN_KEY = "loggedIn"
let TOKEN_KEY = "token"
let USER_EMAIL = "userEmail"


let HEADER = [
    "Content-Type" : "Application/json; charset=utf-8"
]

let BEARER_HEADER = [
    "Authorization" : "Bearer \(AuthService.instance.AuthToken)",
    "Content-Type" : "Application/json; charset=utf-8"
]



